﻿using Microsoft.Extensions.DependencyInjection;
using SimpleApp.Services;
using SimpleApp.ViewModels;
using System;

namespace SimpleApp
{
    public static class Ioc
    {
        private static readonly IServiceProvider _provider;

        static Ioc()
        {
            var services = new ServiceCollection();
            services.AddTransient<HabrParser>();
            services.AddTransient<MainViewModel>();
            services.AddSingleton<ParserActor>();
            services.AddSingleton<EventBus>();


            _provider = services.BuildServiceProvider();
        }

        public static T Resolve<T>() => _provider.GetRequiredService<T>();
    }

    public class ViewModelLocator
    {
        public MainViewModel MainViewModel => Ioc.Resolve<MainViewModel>();
    }
}
