﻿using DevExpress.Mvvm;
using SimpleApp.Services;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SimpleApp.ViewModels
{
    public class MainViewModel : BindableBase
    {
        private readonly HabrParser _parser;
        private readonly ParserActor _parserActor;

        public ObservableCollection<HabrPost> HabrPosts { get; set; } = new ObservableCollection<HabrPost>();
        public MainViewModel(HabrParser parser, ParserActor parserActor, EventBus eventBus)
        {
            _parser = parser;
            _parserActor = parserActor;

            eventBus.Subscribe(OnPageParsed);
        }

        private async Task OnPageParsed(HabrPage page)
        {
            await App.Current.Dispatcher.InvokeAsync(() =>
            {
                foreach (var item in page.Posts)
                    HabrPosts.Add(item);
            });
        }

        private AsyncCommand _Start;
        public AsyncCommand Start => _Start ??= new AsyncCommand(async () =>
        {
            Start.CancellationTokenSource.Token.Register(() => _parserActor.Stop());

            await foreach (var url in _parser.GetUrlsForParse())
            {
                if (Start.IsCancellationRequested) break;

                await _parserActor.SendAsync(url);
            }
        });

    }
}
