﻿using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SimpleApp.Services
{
    public class HabrPage
    {
        public HabrPage(string url, HabrPost[] posts)
        {
            Url = url;
            Posts = posts;
        }

        public string Url { get; }
        public HabrPost[] Posts { get; }
    }

    public class HabrPost
    {
        public string Url { get; }
        public string Title { get; }

        public HabrPost(string url, string title)
        {
            Url = url;
            Title = title;
        }
    }

    public class HabrParser
    {
        private readonly HttpClient _http;
        public HabrParser()
        {
            _http = new HttpClient();
        }

        public async IAsyncEnumerable<string> GetUrlsForParse()
        {
            for (int i = 0; i < 20; i++)
            {
                await Task.Delay(100);
                yield return $"https://habr.com/ru/flows/develop/page{i + 1}/";
            }
        }
        
        public async Task<HabrPage> ParsePage(string url)
        {
            var body = await _http.GetStringAsync(url);

            var html = new HtmlParser();
            var doc = await html.ParseDocumentAsync(body);

            var items = doc.QuerySelectorAll(".post__title_link");

            var urls = items.Select(s => s.GetAttribute("href")).ToArray();
            var titles = items.Select(s => s.Text()).ToArray();

            var posts = items.Select(s =>
            {
                var url = s.GetAttribute("href");
                var title = s.Text();

                return new HabrPost(url, title);
            });

            return new HabrPage(url, posts.ToArray());
        }
    }
}
