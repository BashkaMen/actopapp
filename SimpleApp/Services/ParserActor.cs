﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace SimpleApp.Services
{
    public class ParserActor : AbstractActor<string>
    {
        private readonly HabrParser _parser;
        private readonly EventBus _eventBus;

        public override int ThreadCount => 10;

        public ParserActor(HabrParser parser, EventBus eventBus)
        {
            _parser = parser;
            _eventBus = eventBus;
        }

        public override async Task HandleMessage(string url)
        {
            var page = await _parser.ParsePage(url);
            await _eventBus.RaiseOnPageParsed(page);
        }

        public override async Task HandleError(string url, Exception ex)
        {
            Debug.WriteLine($"Error handling {url} with error {ex.Message}");

            await Task.Delay(1000);

            await SendAsync(url);
        }
    }
}
