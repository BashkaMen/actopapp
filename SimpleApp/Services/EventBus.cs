﻿using System;
using System.Threading.Tasks;

namespace SimpleApp.Services
{
    public class EventBus
    {
        public event Func<HabrPage, Task> OnPageParsed;



        public Task RaiseOnPageParsed(HabrPage page) => OnPageParsed?.Invoke(page) ?? Task.CompletedTask;
        public void Subscribe(Func<HabrPage, Task> handler)
        {
            OnPageParsed += handler;
        }

    }
}
